#define _BSD_SOURCE

#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

#define DEVICE 1
#define DEVICE_ADDR 0x27
#define ASSERT(expr) if ((expr) < 0) { perror("Assertion failed"); exit(1); }

unsigned short backlight = 1;
unsigned short ectl      = 0;

int char_dump(unsigned char c, char *b) {
	for (int i = 0; i < 8; i++) {
		b[7-i] = (c & 1) + '0';
		c >>= 1;
	}
	b[8] = '\0';
}

int write_fd(int fd, unsigned char c, unsigned char rs, unsigned char rw) {
	char buffer[9];

	unsigned char w1 = (c & 0xf0) | (rs & 1) | (rw & 1) << 1 | (ectl & 1) << 2 | (backlight & 1) << 3;
	unsigned char w2 = ((c & 0x0f) << 4) | (rs & 1) | (rw & 1) << 1 | (ectl & 1) << 2 | (backlight & 1) << 3;

	fprintf(stderr, "d: 0x%x, rs: 0x%x, rw: 0x%x\n", c, rs, rw);

	char_dump(w1, buffer);
	fprintf(stderr, "W1: 0b%s\n", buffer);

	char_dump(w2, buffer);
	fprintf(stderr, "W2: 0b%s\n", buffer);
	fprintf(stderr, "------------------------------------\n");


	if (write(fd, &w1, 1) != 1) {
		return -1;
	}

	if (write(fd, &w2, 1) != 1) {
		return -1;
	}
	return 0;
}

int main() {
	char path[0x100];
	sprintf(path, "/dev/i2c-%d", DEVICE);
	int fd;
	ASSERT(fd = open(path, O_RDWR));
	ASSERT(ioctl(fd, I2C_SLAVE, DEVICE_ADDR));

	// ASSERT(write_fd(fd, 0b00100000, 0, 0));
	int temp = 0b00101000;
	ASSERT(write(fd, &temp, 1));
	usleep(2000);

	ASSERT(write_fd(fd, 0b00101000, 0, 0));
	usleep(2000);
	ASSERT(write_fd(fd, 0b00001111, 0, 0));
	usleep(2000);
	ASSERT(write_fd(fd, 0b00000110, 0, 0));
	usleep(2000);
	ASSERT(write_fd(fd, 0b01001000, 1, 0)); // H
	usleep(2000);



	


cleanup:
	if (fd >= 0) {
		close(fd);
	}
	return 0;
}
