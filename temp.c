#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#define SDA 30
#define SCL 60
#define GPIO_BUFFER_LENGTH 256

#define DIRECTION_OUT 1
#define DIRECTION_IN  0
#define HIGH_VALUE    1
#define LOW_VALUE     0
#define ACK           LOW_VALUE
#define NACK          HIGH_VALUE

char *SDIRECTION_OUT = "out";
char *SDIRECTION_IN = "in";

int i2c_delay() {
	return 0;
}

#define SDA_HIGH() set_gpio_value(SDA, HIGH_VALUE)
#define SDA_LOW()  set_gpio_value(SDA, LOW_VALUE)
#define SCL_HIGH() set_gpio_value(SCL, HIGH_VALUE)
#define SCL_LOW()  set_gpio_value(SCL, LOW_VALUE)

#define SDA_OUT() set_gpio_direction(SDA, DIRECTION_OUT)
#define SDA_IN()  set_gpio_direction(SDA, DIRECTION_IN)
#define SCL_OUT() set_gpio_direction(SCL, DIRECTION_OUT)
#define SCL_IN()  set_gpio_direction(SCL, DIRECTION_IN)

#define SDA_VAL() read_gpio(SDA)
#define SCL_VAL() read_gpio(SCL)


int write_to_file(char *file, char *data, int length) {
//	fprintf (stderr, "Writing to file %s the data: %s with length: %d\n",
//			file, data, length);

	int fd = open(file, O_WRONLY);
	if (fd < 0) {
		return -1;
	}

	if (write(fd, data, length) != length) {
		return -3;
	}

	if (close(fd) < 0) {
		return -2;
	}

	return 0;
}

int read_from_file(char *file, char *data, int length) {
//	fprintf (stderr, "Reading from file %s\n");
	int fd = open(file, O_RDONLY);
	if (fd < 0) {
		return -1;
	}

	int data_read = 0;
	if ((data_read = read(fd, data, length)) < 0) {
		return -3;
	}

	data[data_read] = '\0';

	if (close(fd) < 0) {
		return -2;
	}

	fprintf (stderr, "Result of reading from file %s is: %s\n", file, data);

	return 0;
}

int set_gpio_direction(int gpio, int val) {
	char file_path[GPIO_BUFFER_LENGTH];
	snprintf(file_path, GPIO_BUFFER_LENGTH, "/sys/class/gpio/gpio%d/direction", gpio);
	char *sval = val == DIRECTION_OUT ? SDIRECTION_OUT : SDIRECTION_IN;
	return write_to_file(file_path, sval, strlen(sval));
}

int get_gpio_direction(int gpio, int *val) {
	char file_path[GPIO_BUFFER_LENGTH];
	char buffer[GPIO_BUFFER_LENGTH];
	snprintf(file_path, GPIO_BUFFER_LENGTH, "/sys/class/gpio/gpio%d/direction", gpio);
	int result = read_from_file(file_path, buffer, GPIO_BUFFER_LENGTH);
	if (strcmp(buffer, SDIRECTION_OUT) == 0) {
		*val = 1;
	} else if (strcmp(buffer, SDIRECTION_IN) == 0){
		*val = 0;
	} else {
		*val = -1; // ERROR
	}
	return result;
}

int set_gpio_value(int gpio, int val) {
	char file_path[GPIO_BUFFER_LENGTH];
	char buffer_value[GPIO_BUFFER_LENGTH];
	snprintf(file_path, GPIO_BUFFER_LENGTH, "/sys/class/gpio/gpio%d/value", gpio);
	snprintf(buffer_value, GPIO_BUFFER_LENGTH, "%d", val);
	return write_to_file(file_path, buffer_value, strlen(buffer_value));
}

int get_gpio_value(int gpio, int *val) {
	char file_path[GPIO_BUFFER_LENGTH];
	char buffer_value[GPIO_BUFFER_LENGTH];
	snprintf(file_path, GPIO_BUFFER_LENGTH, "/sys/class/gpio/gpio%d/value", gpio);
	int result = read_from_file(file_path, buffer_value, GPIO_BUFFER_LENGTH);
	if (sscanf(buffer_value, "%d", val) != 1) {
		*val = -1;
	}
	return result;
}

int read_gpio(int gpio) {
	int val = 0;
	int result = get_gpio_value(gpio, &val);
	if (result < 0 || val < 0) {
		if (result < 0) {
			return result;
		}
		if (val < 0) {
			return val;
		}
	}

	return val;
}

int i2c_start() {
	if (set_gpio_direction(SDA, DIRECTION_OUT) < 0) {
		perror("[I2C Init setquence 1] Error setting SDA direction to out");
		return -1;
	}
	if (set_gpio_direction(SCL, DIRECTION_OUT) < 0) {
		perror("[I2C Init setquence 2 Error setting SCL direction to out");
		return -1;
	}
	if (set_gpio_value(SDA, 1) < 0) {
		perror("[I2C Init setquence 3 Error setting SDA to value 1");
	}
	i2c_delay();
	if (set_gpio_value(SCL, 1) < 0) {
		perror("[I2C Init setquence 4 Error setting SCL to value 1");
	}
	i2c_delay();
	if (set_gpio_value(SDA, 0) < 0) {
		perror("[I2C Init setquence 5 Error setting SDA to value 0");
	}
	i2c_delay();
	if (set_gpio_value(SCL, 0) < 0) {
		perror("[I2C Init setquence 6 Error setting SCL to value 0");
	}
	i2c_delay();
}

int i2c_stop() {
	if (set_gpio_direction(SDA, DIRECTION_OUT) < 0) {
		perror("[I2C Init setquence 1] Error setting SDA direction to out");
		return -1;
	}
	if (set_gpio_direction(SCL, DIRECTION_OUT) < 0) {
		perror("[I2C Init setquence 2 Error setting SCL direction to out");
		return -1;
	}

	if (set_gpio_value(SDA, 0) < 0) {
		perror("[I2C Init setquence 3 Error setting SDA to value 0");
	}
	i2c_delay();
	if (set_gpio_value(SCL, 1) < 0) {
		perror("[I2C Init setquence 4 Error setting SCL to value 1");
	}
	i2c_delay();
	if (set_gpio_value(SDA, 1) < 0) {
		perror("[I2C Init setquence 5 Error setting SDA to value 1");
	}
	i2c_delay();

}

int i2c_tx(char c) {
	if (c > 0) {
		SDA_HIGH();
	} else {
		SDA_LOW();
	}

	SCL_HIGH();
	i2c_delay();
	SCL_LOW();
	i2c_delay();

	if (c > 0) {
		SDA_LOW();
	}
	
	return 0;
}

int i2c_rx() {
	SDA_HIGH();
	SCL_HIGH();
	i2c_delay();

	int c = SDA_VAL();
	SCL_LOW();
	i2c_delay();
	
	return c;	
}

int i2c_write(unsigned char c) {
//	fprintf (stderr, "Byte: %d\n", c);
	SDA_OUT();
	SCL_OUT();
	for (int i = 0; i < 8; i++) {
		int bit = c & 128;
//		fprintf (stderr, "Bit: %d\n", bit);
		i2c_tx(bit);
		c <<= 1;
	}
	SCL_HIGH();
	SDA_IN();
	int b = SDA_VAL();
	if (b == NACK) {
		fprintf (stderr, "Slave did not confirmed\n");
	}
	SCL_LOW();
}

int i2c_read(unsigned char ack) {
	SDA_IN();
	SCL_OUT();
	unsigned char res = 0;
	for (int i = 0; i < 8; i++) {
		res <<= 1;
		res |= i2c_rx();
	}

	SDA_OUT();
	if (ack >= HIGH_VALUE) {
		i2c_tx(LOW_VALUE);
	} else {
		i2c_tx(HIGH_VALUE);
	}
	i2c_delay();
	return 0;
}

int main() {
	fprintf(stderr, "Starting i2c\n");
	if (i2c_start() < 0) {
		fprintf (stderr, "Starting i2c failed\n");
		return 1;
	}
	fprintf (stderr, "Writing clear command\n");
	if (i2c_write(0x27) < 0) {
		fprintf (stderr, "Error command\n");
	}

	if (i2c_write(0x0) < 0) {
		fprintf (stderr, "Error command\n");
	}

	fprintf(stderr, "Stopping i2c\n");
	if (i2c_stop() < 0) {
		fprintf (stderr, "Stopping i2c failed\n");
	}
	return 0;
}
