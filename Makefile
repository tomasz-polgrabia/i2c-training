CFLAGS=-std=c99
LFLAGS=

all: i2c

temp: temp.c.o
	gcc -o temp $^ $(LFLAGS)

%.c.o: %.c
	gcc $(CFLAGS) -c -o $@ $<

i2c: i2c.c.o
	gcc -o $@ $^ $(LFLAGS)
